import os,sys,re
import pdb,traceback,warnings

import argparse
from pprint import pprint

parser = argparse.ArgumentParser()
parser.add_argument('texts', nargs='+', help='text strings to search for')
#parser.add_argument('--milestone', '-m', action='store_true', help='iterate to the next milestone (second-order) version update')
#parser.add_argument('--explicit', '-e', help='specify an explicit full version number')
args = parser.parse_args()



sys.path.append('.')
import frenchapp

def run_program():
    for text in args.texts:
        examples = frenchapp.gen.find_examples(text)
        print(f"-------------------------------\nSEARCH: \"{text}\"\n\n{len(examples)} Results:")
        pprint(examples)

try:
    run_program()
except:
    errtype,errvalue,errtb = sys.exc_info()
    traceback.print_exc()
    pdb.post_mortem(errtb)
