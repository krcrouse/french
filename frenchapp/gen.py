import flask
import string, random
import re
import urllib.parse
import html
import sys
import verbecc
import yaml
import googlesearch
from werkzeug.exceptions import abort
from bs4 import BeautifulSoup
import requests
import unicodedata
import urllib.request

bp = flask.Blueprint('gen', __name__, url_prefix='/gen')

@bp.route('/examples')
def examples_endpoint():
    """ Returns some sentence examples of the listed phrase"""
    params = parse_parameters()
    if not params.get('textstring'):
        abort(400, "Cannot call examples without a textstring")
    kwargs = {
        'textstring': params['textstring']
    }
    if 'target_count' in params and params['target_count']:
        kwargs['target_count'] = params['target_count']
    examples = find_examples(**kwargs)
    
    return({
        'examples': examples,
        'count': len(examples),
    })
    

def find_examples(textstring, target_count=8, sites=[ 'lemonde.fr', 'ledevoir.com', 'lapresse.ca']):

    examples = []
    example_texts = []

    #print(f"Search for \"{textstring}\" from {sites} (target count {target_count})")
    search = googlesearch.search(
        f'"{textstring}" (site:' + " OR site:".join(sites) + ')',  
        lang="fr",
        user_agent="Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4)",
    )

    regstr = re.sub(r'\s+', r'\\s+', textstring)
    rex = re.compile(rf'([^\.\!\?\n\t]+{regstr}[^\.\!\?\n\t]+[\.\!\?])')
    startre = re.compile(r'^[\s\'"]+')
    endre = re.compile(r'[\s\'"]+$')
    spacere = re.compile(r'\s{3,}')
    sitre = re.compile(r'(https?://[^\/]+)', re.I)
    
    for url in search:
        #print(f"URL: {url}")
        resp = requests.get(url)    
        if not resp.ok:
            #print(f"\tFailed with status {resp.status_code}")
            continue
        soup = BeautifulSoup(resp.text, 'html.parser')
        text = unicodedata.normalize('NFKC', soup.get_text())
        found = rex.findall(text)
        m = sitre.match(url)
        site = m.group(1)
        for possible in found:
            possible = re.sub(endre, '', re.sub(startre,'',possible))
            # some html gets weird, so we skip it if there are lots of adjacent spaces
            if spacere.search(possible):
                #print(f"\tToo many spaces, skipping [{possible}]")
                continue
            if possible in example_texts:
                #print(f"\tDuplicate! Skipping.")
                continue
            
            example_texts.append(possible)
            examples.append({
                'text':possible,
                'href': url,
                'site': site,
            })
        if len(examples) >= target_count:
            return(examples)
        #print(f"Continue along... we only have {len(examples)} examples")
    return(examples)


def parse_parameters(param_items=None, checkbox_params=None):
    """ Render the url/form/data parameters to create a python dict of it.
    
    This ends up being different from calling `flask.request.values.to_dict()` because:
    - It translate the empty string to None
    - It handles wonky serialization of certain form elements. When `jQuery.serialize()` encounters checkboxes, it passes checked checkboxes as emptry strings, and False checkboxes are not included at all. So, we translate that to an accurate boolean.

    Args:
        param_items (dict): A dictionary of parameter items to parse. The default, which is appropriate for flask-webpages contexts, is flask.request.values
        checkbox_params (list): A list of form items to translate. If they exist in the incoming parameter, their value will be set to true. If they do not exist, they will be added and set to False.  
    """
    if param_items is None:
        param_items = flask.request.values

    parameters = {k:None if v == '' else v for k,v in param_items.items()}
    #
    # jQuery.serialize is dumb. Checked checkboxes are encoded as empty hash values, and unchecked checkboxes are not present in the paraemeter list.
    # So we fix that here
    if checkbox_params:
        for checkboxparam in checkbox_params:
            parameters[checkboxparam] = checkboxparam in parameters
    return(parameters)


