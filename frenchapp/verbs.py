import flask
import string, random
import re
import urllib.parse
import html
import sys
import verbecc
import yaml
import googlesearch

cg = verbecc.Conjugator(lang='fr')

with open('data/verbs-200.yml') as fh:
    topverbs = yaml.safe_load(fh)

bp = flask.Blueprint('verbs', __name__, url_prefix='/verbs')

@bp.route('/conjugator')
def conjugator():
    """ The endpoint for the Google Document importer. If a GET request, returns the web form. POST is expected to be an ajax call to submit the import."""
    return(flask.render_template('verbs/conjugator.html'))

@bp.route('/getverb')
def getverb():
    return(select_conjugation())

def select_conjugation(
    moods=[
        ('conditionnel', 'présent'),
        ('imperatif', 'imperatif-présent'),
        ('indicatif', 'futur-simple'),
        ('indicatif', 'imparfait'),
        ('indicatif', 'passé-composé'),
        #('indicatif', 'passé-simple'),
        ('indicatif', 'présent'),
    ]):
    # -- get a random verb
    vb, definition = topverbs[random.randrange(len(topverbs))]
    verb = cg.conjugate(vb)

    selected_mood = moods[random.randrange(len(moods))]

    options = verb['moods'][selected_mood[0]][selected_mood[1]]
    if len(options) == 0:
        # in case the verb doesn't actually have the listed conjugation style, repeat
        return(select_conjugation(moods=moods))

    conj = options[random.randrange(len(options))]

    result = {
        'infinitive': verb['verb']['infinitive'],
        'template': verb['verb']['template'],
        'definition': definition,
        'stem': verb['verb']['stem'],
        'full': verb['moods'],
        'selected_mood': selected_mood[0],
        'selected_tense': selected_mood[1],
        'selected_conjugation': conj,
        'selected_set': options,
    }
    return(result)

