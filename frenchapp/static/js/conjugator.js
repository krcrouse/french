$(document).ready(function () {
    $('#next').click(newConjugation);
    $('#get-examples').click(getExamples);
        
        
    newConjugation();
});

function reset(){
    /* make sure all the collapsed things are collapsed still*/
    $('.collapse').removeClass('show');
    $('.accordion-collapse').removeClass('show');
    $('#examples').html('');
}

function getExamples(){
    $('#examples').html('<div id="example-spinner" class="text-center"><div class="spinner-border text-primary" role="status"><span class="visually-hidden">Loading...</span></div></div>');
    $.ajax({
        url: "/gen/examples",
        method: "GET",
        dataType: "json",
        data: {
            textstring: $('#selected-element').html()
        },
        cache: false,
        error: ajaxError,
        success: function(data) {  
            if(data.count == 0){
                $('#examples').html('<div class="alert alert-warning" role="alert">We could not retrieve any examples!</div>');
            }
            else{
                var list = $('<ul class="list-group"></ul>');
                $.each(data.examples, function(i, ref){
                    list.append('<li class="list-group-item fst-italic fw-light">' + ref.text + ' (<a href="'+ref.href+'" target="_blank">' + ref.site + '</a>)');
                });
                $('#examples').html(list);
            }
        }
    });
}

function newConjugation(){
    $.ajax({
        url: "/verbs/getverb",
        method: "GET",
        dataType: "json",
        data: {},
        cache: false,
        error: ajaxError,
        success: function(data) {  

            reset();
            /* main information */   
            $('#selected-element').html(data.selected_conjugation);  
            $('#selected-mood').html(data.selected_mood);  
            $('#selected-tense').html(data.selected_tense);  
            $('#infinitive').html(data.infinitive);  
            $('#definition').html(data.definition);  
                
            /* the other conjugated items for this tense */
            var full_mood_conjugation = [];
            $.each(data.selected_set, function(i,v){
                if(v == data.selected_conjugation)
                    v = '<mark>' + v + '</mark>';
                full_mood_conjugation.push($('<li class="conjugation">' + v + '</li>'));
            });
            $('#tense-conjugation').html(full_mood_conjugation);

            /* the full conjugation model for this verb */
            var full_conjugation = [];
            var imood = 0;
            $.each(data.full, function(mood, tenses){
                if(mood == 'infinitif')
                    return;
                imood++;
                var tenses_accordian = $('<div class="accordian" id="mood-accordian-'+imood+'"></div>');
                var itense = 0;
                $.each(tenses, function(tense, conjugations){
                    if(!conjugations.length)
                        return;
                    itense++;
                    var tenseid = imood + '-' + itense;
                    var tense_ul = $('<ul class="list-elegant offset-sm-1"></ul>');
                    $.each(conjugations, function(i,conj){
                        if(conj == data.selected_conjugation)
                            conj = '<mark>' + conj + '</mark>';
                        tense_ul.append($('<li class="conjugation">' + conj + "</li>"));
                    })

                    var tense_body = 
                    $('<div id="tense-'+tenseid+'"class="accordion-collapse collapse" aria-labelledby="tense-heading-'+tenseid+'" data-bs-parent="#mood-accordian-'+imood+'">').append(
                        $('<div class="accordian-body"></div>').append(tense_ul)
                    );
                    var tense_class = tense == data.selected_tense ? ' current-verb' : '';


                    var tense_element = $('<div class="accordion-item tense-item"><h2 class="accordion-header" id="tense-heading-'+tenseid+'"><button class="accordion-button'+tense_class+'" type="button" data-bs-toggle="collapse" data-bs-target="#tense-'+tenseid+'" aria-expanded="true" aria-controls="tense-'+tenseid+'">' + tense + '</button></h2></div>').append(tense_body);

                    tenses_accordian.append(tense_element);
                });

                var mood_body = 
                $('<div id="mood-'+imood+'"class="accordion-collapse collapse offset-sm-1" aria-labelledby="mood-heading-'+imood+'" data-bs-parent="#full-conjugation">').append(
                    $('<div class="accordian-body"></div>').append(tenses_accordian)
                );
                var mood_class = mood == data.selected_mood ? ' current-verb' : '';
                    
                var mood_element = $('<div class="accordion-item"><h2 class="accordion-header" id="mood-heading-'+imood+'"><button class="accordion-button'+mood_class+'" type="button" data-bs-toggle="collapse" data-bs-target="#mood-'+imood+'" aria-expanded="true" aria-controls="mood-'+imood+'">' + mood + '</button></h2></div>').append(mood_body);


                full_conjugation.push(mood_element);
            });

                /*
            {
                To do it in list version
                var mood_ul = $('<ul class="list-elegant"></ul>');
                for (var tense in data.full[mood]){
                    var tense_ul = $('<ul class="list-elegant"></ul>');
                    $.each(data.full[mood][tense], function(i,conj){
                        if(conj == data.selected_conjugation)
                            conj = '<mark>' + conj + '</mark>';
                        tense_ul.append($('<li class="conjugation">' + conj + "</li>"));
                    })
                    var tense_item = $('<li class="tense-container"><span class="tense label">' + (tense == data.selected_tense ? '<mark>' + tense + '</mark>' : tense) + "</span></li>");
                    tense_item.append(tense_ul);
                    mood_ul.append(tense_item);
                }
                var mood_item = $('<li class="mood-container"><span class="mood label">' + (mood == data.selected_mood ? '<mark>' + mood + '</mark>' : mood)+ "</span></li>");
                mood_item.append(mood_ul);
                full_conjugation.push(mood_item);
            }
            */
            $('#full-conjugation').html(full_conjugation);
            // All verbs conjugated in the same mood - tense 
            //<div id="full-conjugation">
            //</div>
        }
    });
}

